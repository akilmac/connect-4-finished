/* Uses some Code from Randys Demo

1. Initialize the game
    - players
    - board display
    - board model
    - current player tracker
    - click handlers for each column
2. Take player input
    - know which player is dropping a disc
    - which column are we dropping into?
    - is that column already full?
    - drop the disc into the top of the column
3. Check for game end conditions
    - tie
    - win
    - announce that the game is over

*/

const boardModel = [
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ],
    [ null, null, null, null, null, null, null ]
]

let currentPlayer = null // 1 or 2
let numberOfDiscsDropped = 0

// Akil Mcelhannon
const displayMessage = function (message) {
    // Clear out the message div
    // Add new message to div
    document.getElementById("message").innerHTML = message;
}


const displayCurrentPlayer = function (currentPlayer) {
    if (currentPlayer === null){
        displayMessage("Choose who goes first &#10230;");
    } else if (currentPlayer === 1){
        displayMessage("Current Player: Red")
    } else if (currentPlayer === 2) {
        displayMessage("Current Player: Yellow")
    }
}
// Akil Mcelhannon
const isColumnFull = function (columnNum) {
    // Look at the boardModel to determine if col is full
    if (document.getElementById("col" + columnNum).childElementCount == 6) {
        return true
    } else {
        return false
    }
}

// Ian Waddell
const dropDisc = function (columnNum) {                
    let gameStatus = isGameOver(boardModel)
    let col = document.getElementById("col" + columnNum)

    if(gameStatus !== "win") {

        //Add a disc to the DOM for the current player
        if (currentPlayer === 1) {
            col.innerHTML += '<div class="disc red"></div>'
        } else if (currentPlayer === 2) {
            col.innerHTML += '<div class="disc yellow"></div>'
        }
        
        // Add a disc to the boardModel
        for (let i = col.childElementCount - 1; i >= 0; i--) {
            if (boardModel[i][columnNum] === null) {
                boardModel[i][columnNum] = currentPlayer;
            }
        }
        
        numberOfDiscsDropped++   
    } 
}


const isGameOver = function (boardModel) {
    // Check for a win
    // Check for a tie (numberofDiscsDropped === 42)
    let win = null;

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 7; j++) {
          if (
            boardModel[i][j] == boardModel[i + 1][j] &&
            boardModel[i][j] == boardModel[i + 2][j] &&
            boardModel[i][j] == boardModel[i + 3][j] &&
            boardModel[i][j] !== null
          ) {win = "yes";} else if (
            boardModel[i][j] == boardModel[i][j + 1] &&
            boardModel[i][j] == boardModel[i][j + 2] &&
            boardModel[i][j] == boardModel[i][j + 3] &&
            boardModel[i][j] !== null
            ) {win = "yes";} else if (
                boardModel[i][j] == boardModel[i + 1][j - 1] &&
                boardModel[i][j] == boardModel[i + 2][j - 2] &&
                boardModel[i][j] == boardModel[i + 3][j - 3] &&
                boardModel[i][j] !== null
            ) {win = "yes";} else if (
                boardModel[i][j] == boardModel[i + 1][j + 1] &&
                boardModel[i][j] == boardModel[i + 2][j + 2] &&
                boardModel[i][j] == boardModel[i + 3][j + 3] &&
                boardModel[i][j] !== null
            ) {win = "yes";} 
        }
      }

    if (win === "yes") {
        return "win"
    } else if (numberOfDiscsDropped === 42) {
        return "tie"
    } else {
        return false;
    }
    
}

const displayTieMessage = function () {
    displayMessage("Tie game!")
}

const displayWinMessage = function () {
    if (currentPlayer === 1) {
        displayMessage("Red Wins!!!")
    } else {
        displayMessage("Yellow Wins!!!")
    }
}


const switchToNextPlayer = function () {
    //     TODO: Toggle currentPlayer variable 1<-->2
    if (currentPlayer === 1) {
        currentPlayer = 2
        displayCurrentPlayer(currentPlayer)
    } else if (currentPlayer === 2){
        currentPlayer = 1;
        displayCurrentPlayer(currentPlayer)
    }
}

const columnClickHandler = function (eventObj) {
    const selectedCol = eventObj.currentTarget;
    const columnNum = Number(selectedCol.id.slice(-1));

    if (isColumnFull(columnNum)) {
        // display a message saying they can't drop there
        document.getElementById("message2").innerHTML = "Column full, can't drop disc there"
    } else {
        dropDisc(columnNum);

        if(currentPlayer === 1){
            document.getElementById("message2").innerHTML = "Red dropped a disc onto Column " + (columnNum + 1)
        }
        else if (currentPlayer === 2) {
            document.getElementById("message2").innerHTML = "Yellow dropped a disc onto Column " + (columnNum + 1)
        }
        

        let gameStatus = isGameOver(boardModel)
        if (gameStatus === "tie") {
            displayTieMessage()
        } else if (gameStatus === "win") {
            displayWinMessage();
            if(currentPlayer === 1){document.getElementById("message2").innerHTML = "Red Wins!!!"}
        else {document.getElementById("message2").innerHTML = "Yellow wins!!!"}
        } else {
            switchToNextPlayer()
        }
    }
}

const setUpEventListeners = function () {
    document.querySelector('#col0').addEventListener('click', columnClickHandler)
    document.querySelector('#col1').addEventListener('click', columnClickHandler)
    document.querySelector('#col2').addEventListener('click', columnClickHandler)
    document.querySelector('#col3').addEventListener('click', columnClickHandler)
    document.querySelector('#col4').addEventListener('click', columnClickHandler)
    document.querySelector('#col5').addEventListener('click', columnClickHandler)
    document.querySelector('#col6').addEventListener('click', columnClickHandler)
}

const initializeGame = function () {
    setUpEventListeners()
    displayCurrentPlayer(currentPlayer)


    // Choose who goes first
    if (currentPlayer === null) {
       document.getElementById("red").addEventListener('click', p1Choice)
       document.getElementById("ylw").addEventListener('click', p1Choice)
    }
}

function p1Choice(e) {
    if(e.currentTarget.id === "red") {
        currentPlayer = 1;
        document.getElementById("red").removeEventListener('click', p1Choice);
        document.getElementById("ylw").removeEventListener('click', p1Choice);
        displayCurrentPlayer(currentPlayer)
        color_choice.remove();
    } else if (e.currentTarget.id === "ylw") {
        currentPlayer = 2;
        document.getElementById("red").removeEventListener('click', p1Choice);
        document.getElementById("ylw").removeEventListener('click', p1Choice);
        displayCurrentPlayer(currentPlayer)
        color_choice.remove();
    }
}

initializeGame()

function resetGame(){
    let reset = confirm("Reset the game?");
    if (reset == true){
    location.reload(false);
    } 
}